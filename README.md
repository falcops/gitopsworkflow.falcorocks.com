# Gitops workflow

* pull based, uses the kubernetes agent
* redeploys https://gitopsworkflow.falcorocks.com

### pros of gitops workflow
* deployments with no maintenance, the agent does everything for us

### cons
* as is this works only for static manifests, which is very limited
* extra logic needed to replace deployed image (e.g. with new tag). This could be done through a simple job that runs this `kubectl set image deployment/<deployment name> <container>=<image>:<registyr>` through a cicd tunnel, with child parent pipelines or with a downstream job in a new project
